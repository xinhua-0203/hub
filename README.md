<div align=center>
<img src="https://gitee.com/itxinfei/hub/raw/master/xinfei.png"/>
</div>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">IT项目Hub</h1>

<p align="center">
	<a href="https://gitee.com/itxinfei/hub"><img src="https://gitee.com/itxinfei/hub/badge/star.svg?theme=gvp"></a>
</p>

```
为成长积蓄力量，努力成为全栈工程师！
```

- QQ：747011882  Q群：<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=-KhniqEHPHRfh04Ui7r9eH2kCEvfrE_-&jump_from=webapi&authKey=gcHlWnSJxgYRuE2XY/FV5k9bDKPwHNtiw9vf1/TKWhyfTHfjAM8oYR71y1fHEUpH">661543188<img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="Java项目交流" title="Java项目交流"></a>  
- 博客：https://blog.csdn.net/qq_20401183
- B站： https://space.bilibili.com/437960072

# 一、黑马程序员
- 1、瑞吉外卖
https://gitee.com/itxinfei/reggie.git
- 2、苍穹外卖
https://gitee.com/itxinfei/sky.git
- 3、客达天下
https://gitee.com/itxinfei/crm.git
- 4、学成在线
https://gitee.com/itxinfei/xuecheng-parent.git
- 5、学成在线V3
https://gitee.com/itxinfei/xuecheng-plus-parent
- 6、天机学堂
https://gitee.com/itxinfei/tianji
- 7、品达物流
https://gitee.com/itxinfei/pinda-tms.git
- 8、BOSS物流
https://gitee.com/itxinfei/boss.git
- 9、立可得
https://gitee.com/itxinfei/lkd_parent.git
- 10、帝可得
https://gitee.com/itxinfei/elegent
- 11、餐掌柜
https://gitee.com/itxinfei/restkeeper.git
- 12、SaaS-IHRM
https://gitee.com/itxinfei/ihrm_parent.git
- 13、P2P金融
https://gitee.com/itxinfei/itcast_p2p.git
- 14、万信金融
https://gitee.com/itxinfei/wanxinp2p.git
- 15、十次方
https://gitee.com/itxinfei/tensquare-parent.git
- 16、探花交友
https://gitee.com/itxinfei/tanhua.git
- 17、冰眼冷链
https://gitee.com/itxinfei/cold.git
- 18、元蜂WMS
https://gitee.com/itxinfei/wms.git
- 19、黑马秒杀交易
https://gitee.com/itxinfei/seckill-parent.git
- 20、九点钟
https://gitee.com/itxinfei/nc_parent.git
- 21、集信达
https://gitee.com/itxinfei/jixinda.git
- 22、亿可控
https://gitee.com/itxinfei/yikekong.git
- 23、黑马头条
https://gitee.com/itxinfei/heima-leadnews.git
- 24、充吧
https://gitee.com/itxinfei/chongba-parent.git
- 25、智慧学成
https://gitee.com/itxinfei/xc-analysis.git
- 26、传智健康
https://gitee.com/itxinfei/health_parent.git
- 27、淘淘商城
https://gitee.com/itxinfei/taotao-parent.git
- 28、品优购商城
https://gitee.com/itxinfei/pinyougou-parent.git
- 29、宜立方商城
https://gitee.com/itxinfei/e3-parent.git
- 30、乐优商城
https://gitee.com/itxinfei/leyou-parent.git
- 31、畅购商城
https://gitee.com/itxinfei/changgou-parent.git
- 32、青橙商城
https://gitee.com/itxinfei/qingcheng.git
- 33、好客租房
https://gitee.com/itxinfei/haoke.git
- 34、智维找房
https://gitee.com/itxinfei/houserush.git
- 35、权限管家
https://gitee.com/itxinfei/itcast-authority.git
- 36、闪聚支付（闪聚宝）
https://gitee.com/itxinfei/shanjupay.git
- 37、地图专题
https://gitee.com/itxinfei/itcast-baidumap
- 38、智牛股
https://gitee.com/itxinfei/bulls-stock.git
- 39、红包雨
https://gitee.com/itxinfei/prize.git
- 40、神领物流
https://gitee.com/itxinfei/sl-express.git
- 41、中州养老
https://gitee.com/itxinfei/zzyl
- 42、Hiss 流程中心
https://gitee.com/itxinfei/hiss-center
- 43、云岚到家
https://gitee.com/itxinfei/jzo2o-foundations
- 44、SaaS Export
https://gitee.com/itxinfei/export_parent.git
- 45、黑马顺风车
https://gitee.com/itxinfei/hitch
- 46、汇客CRM
https://gitee.com/itxinfei/huike-parent
- 47、四方保险 https://gitee.com/itxinfei/sfbx

# 二、尚硅谷
- 1、尚品汇（谷粒商城）https://gitee.com/itxinfei/gmall.git
- 2、尚课吧（谷粒学院）https://gitee.com/itxinfei/guli_parent.git
- 3、尚医通 https://gitee.com/itxinfei/yygh-parent.git
- 4、尚融宝 https://gitee.com/itxinfei/shangrongbao.git
- 5、尚筹网 https://gitee.com/itxinfei/atcrowdfunding.git
- 6、美年旅游 https://gitee.com/itxinfei/meinian_parent

# 三、尚学堂
- 1、CoinExchange开源数字货币交易所 https://gitee.com/itxinfei/coin-exchange.git

# 四、官方教程
## 1、黑马程序员
- Java：https://pan.baidu.com/s/1mXQiIzRF-MdeWJOcnFRk7Q?pwd=nnt8
- 前端：https://pan.baidu.com/s/15yVOafBdEz3VU6hvYCOsYA?pwd=1234
- Python大数据：https://pan.baidu.com/s/1PkCjWWCF9EEG6KQrC255hA?pwd=1234
- 测试：https://pan.baidu.com/s/1E2nRWGNDLYrYQ01FiBZQvw?pwd=9988
- C++：https://pan.baidu.com/s/1vTXgoXql0JZIuThKIMJJCA?pwd=1234
- 人工智能：https://pan.baidu.com/s/12gWvj_eOoletqQUoncCFpg?pwd=9988
- UI：https://pan.baidu.com/s/1jOuG7lJFC2UMaW5xjDs9cA?pwd=9988
- 综合：https://pan.baidu.com/s/1gMILCY2nmA0rs6_u4Aft7w?pwd=1234

## 2、尚硅谷
- Java：https://pan.baidu.com/share/init?surl=PhTeMkX5vOg0ZRcw0abjCw&pwd=yyds
- 前端：https://pan.baidu.com/share/init?surl=Hx5hWneJQ-4yFteE1iM9qw&pwd=yyds
- 大数据：https://pan.baidu.com/s/18Feqa_63640xPB0fYJ8Ttg，提取码：9bnr
- Linux：https://pan.baidu.com/share/init?surl=qcjDSk5cVzvGk3DKd06BTA&pwd=yyds
- C语言：https://pan.baidu.com/s/1Sy-dVuBjEWY4dwl5tnW4-w?pwd=yyds ?提取码：yyds
- Python：https://pan.baidu.com/s/1FGxdOoUZhuLe-Hee70Dgyw?pwd=yyds 提取码：yyds
- 区块链：https://pan.baidu.com/s/1PJocX5nbpWCwMERMF76vAQ 提取码：yyds
- Go语言：https://pan.baidu.com/s/1WDw-cAKK35Ays6msablyGg 提取码：d981
- Android：https://pan.baidu.com/s/1CoJMmNGvI6IVSsWyzB7m4g?pwd=yyds 提取码：yyds
